<?php
session_start();
 require_once 'libs/Smarty.class.php';
 require_once 'Language.php';
 $smarty = new Smarty();
 if(isset($_SESSION['admin']))
 	$smarty->assign("as_admin", $_SESSION['admin']);
 else
 	$smarty->assign("as_admin", 0);
 if(!isset($_SESSION['language']))
 	$_SESSION['language'] = 'ua';
 $smarty->assign("title",$title);
 $smarty->display('header.tpl');
?>