<?php
 require_once "PostItem.php";
 require_once "PostDao.php";
 require_once 'libs/Smarty.class.php';
 $title = 'Post';
 include 'header.php';
 $dao = new PostDAO();
 if($_SERVER['REQUEST_METHOD']=='POST'){
 	$dao -> editPost($_POST['id'], $_POST['title_ru'], $_POST['title_ua'], $_POST['desc_ru'], $_POST['desc_ua'], $_POST['content_ru'], $_POST['content_ua'], $_POST['type']);
 	echo '<script type="text/javascript">'
   , 'window.location="../Blog/admin.php";'
   , '</script>';
}else{
	$post = $dao -> getPost($_GET['id']);
	$smarty = new Smarty();
	$smarty->assign("id", $post->getId());
	$smarty->assign("title_ru",$post->getTitleRu());
	$smarty->assign("content_ru", $post ->getContent_ru());
	$smarty->assign("title_ua",$post->getTitleUa());
	$smarty->assign("content_ua", $post -> getContent_ua());
	$smarty->assign("desc_ru", $post -> getDesc_ru());
	$smarty->assign("desc_ua", $post -> getDesc_ua());
	$smarty->assign("type", $post -> getType());
	$smarty->display('edit.tpl');
}
 include 'footer.php';
?>