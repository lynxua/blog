<?php
 require_once "PostItem.php";
 require_once "PostDao.php";
 $title = 'Admin';
 include 'header.php';
 if(!isset($_SESSION['admin'])||$_SESSION['admin']!=1){
	if($_SERVER['REQUEST_METHOD']=='GET'){
	 	echo("<div class=\"container\">
	    <div class=\"row\">
	        <div class=\"col-md-offset-5 col-md-3\">
		        <div class=\"form-login\">
		            <h4>Welcome back.</h4>
		            <form action=\"admin.php\" method=\"post\">
			            <input name=\"password\" type=\"text\" id=\"userPassword\" class=\"form-control input-sm chat-input\" placeholder=\"password\" />
			            </br>
			            <div class=\"wrapper\">
			            <span class=\"group-btn\">
			                <button type=\"submit\" class=\"btn btn-primary btn-md\">login <i class=\"fa fa-sign-in\"></i></button>
			            </span>");
			            if(isset($_GET['error'])&&$_GET['error']==1) echo('<p>Illegal password</p>');
		echo("			</form>
		            </div>
		        </div>
	        </div>
	    </div>
		</div>");
	}
	else if($_SERVER['REQUEST_METHOD']=='POST'){
		if($_POST['password']=="12345"){
	 		$_SESSION['admin'] = 1;
	 		echo '<script type="text/javascript">'
   			, 'window.location="../Blog/admin.php";'
   			, '</script>';
	 	}else{
	 		echo '<script type="text/javascript">'
   			, 'window.location="../Blog/admin.php?error=1";'
   			, '</script>';
	 	}
	}
 }else{
 	$dao = new PostDAO();
 	$dao->getAllPosts(true);
 }
 include 'footer.php';
 // <input type=\"text\" id=\"userName\" class=\"form-control input-sm chat-input\" placeholder=\"username\" />
            // </br>
?>