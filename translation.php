<?php
require_once "TranslationItem.php";
 require_once "PostDao.php";
 	$dao = new PostDAO();
 // if(!isset($_SESSION['admin'])){
	if($_SERVER['REQUEST_METHOD']=='GET'){
		$title = 'Translation';
		include 'header.php';
		echo "<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>";
	 	echo "<script>  
	            function show()  
	            {  
	                $.ajax({  
	                    url: \"translation.php\", 
	                    type: \"POST\", 
	                    cache: false,  
	                    success: function(html){  
	                        $(\"#translation\").html(html);  
	                    }  	
	                });  
	            }
			    $(document).ready(function() {
			      $('#submit').click(function(event) {
			        var text_ru=$('#text_ru').val();
			        var text_ua=$('#text_ua').val();
			        $.post('add_translation_item.php',{text_ru:text_ru,text_ua:text_ua});
			        show();
			      });
					$('#clear').click(function(event) {
			        $.post('clear_translation.php');
			        show();
			      });
			    });
	          
	            $(document).ready(function(){  
	                show();  
	                setInterval('show()',5000);  
	            });  
	        </script>";
		echo "<div id=\"translation\">";
		$dao -> getTranslation();
		echo "</div>";
		if(isset($_SESSION['admin'])&&$_SESSION['admin']==1){
			$smarty = new Smarty();
			$smarty->display('add_translation_item.tpl');
		}
		 include 'footer.php';
	}
	else if($_SERVER['REQUEST_METHOD']=='POST'){
		$dao -> getTranslation();
	}
?>