
<?php
	require_once 'libs/Smarty.class.php';
	$smarty = new Smarty();
	if(!isset($_SESSION['language']))
		$_SESSION['language'] = 'ua';
	$smarty->assign("language", $_SESSION['language']);
	$smarty->display('footer.tpl');
?>