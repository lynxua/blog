<?php
class PostItem
{
private $id;
private $date;
private $text_ru;
private $text_ua;

public function __construct($id, $date, $text_ru, $text_ua)
{
	$this->id = $id;
	$this->date = $date;
	$this->text_ru = $text_ru;
	$this->text_ua = $text_ua;
}

public function getId()
{
	return $this->id;
}

public function getDate()
{
	return $this->date;
}

public function getTextRu()
{
	return $this->text_ru;
}

public function getTextUa()
{
	return $this->text_ua;
}
public function __toString()
{
	return "id = $this->id;<br /> 
	date = $this->date;<br /> 
	textru = $this->text_ru;<br /> 
	textua = $this->text_ua;<br /> ";
}
}
?>