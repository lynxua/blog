<?php
require_once "PostItem.php";
 require_once "PostDao.php";
 require_once 'libs/Smarty.class.php';
 $title = 'Delete post';
 include 'header.php';
 $dao = new PostDAO();
 if($_SERVER['REQUEST_METHOD']=='POST'){
 	$dao -> deletePost($_POST['id']);
 	echo '<script type="text/javascript">'
   , 'window.location="../Blog/admin.php";'
   , '</script>';
}
include 'footer.php';
?>