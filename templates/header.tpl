	<!DOCTYPE html>
	<html>
	<head>
	<title>{$title} | Lynx's blog</title>
	<link rel="stylesheet" href="style/style.css"/>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
	</head>
	<body>
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="../Blog/index.php">Lynx's blog</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../Blog/admin.php">{Translation::getLabel('admin_panel')}<span class="sr-only">(current)</span></a></li>
        <li><a href="../Blog/translation.php">{Translation::getLabel('translation')}</a></li>
        <li><a href="../Blog/about.php">{Translation::getLabel('about')}</a></li>
      </ul>
      {if $as_admin eq 1}
      <ul class="nav navbar-nav navbar-right">
        <li><a href="exit.php">{Translation::getLabel('exit')}</a></li>
      </ul>
      {/if}
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>