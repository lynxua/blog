<div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="edit.php" method="post">
          <input type="hidden" name="id" value="{$id}"/>
          <fieldset>
            <legend class="text-center">Edit post</legend>
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Russian title</label>
              <div class="col-md-9">
                <input id="name" name="title_ru" type="text" placeholder="Russian title" class="form-control" value="{$title_ru}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Ukarainian title</label>
              <div class="col-md-9">
                <input id="name" name="title_ua" type="text" placeholder="Ukarainian title" class="form-control" value="{$title_ua}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Russian description</label>
              <div class="col-md-9">
                <input id="name" name="desc_ru" type="text" placeholder="Russian description" class="form-control" value="{$desc_ru}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Ukrainian description</label>
              <div class="col-md-9">
                <input id="name" name="desc_ua" type="text" placeholder="Ukrainian description" class="form-control" value="{$desc_ua}">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Type</label>
              <div class="col-md-9">
                <input id="name" name="type" type="text" placeholder="Type" class="form-control" value="{$type}">
              </div>
            </div>
    
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Russian content</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ru" placeholder="Content..." rows="5">{$content_ru}</textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Ukrainian content</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ua" placeholder="Content..." rows="5" >{$content_ua}</textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>