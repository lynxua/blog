<div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="add.php" method="post">
          <fieldset>
            <legend class="text-center">{Translation::getLabel('edit_post')}</legend>
    
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">{Translation::getLabel('title_ru')}</label>
              <div class="col-md-9">
                <input id="name" name="title_ru" type="text" placeholder="{Translation::getLabel('title_ru')}" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">{Translation::getLabel('title_ua')}</label>
              <div class="col-md-9">
                <input id="name" name="title_ua" type="text" placeholder="{Translation::getLabel('title_ua')}" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">{Translation::getLabel('desc_ru')}</label>
              <div class="col-md-9">
                <input id="name" name="desc_ru" type="text" placeholder="{Translation::getLabel('desc_ru')}" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">{Translation::getLabel('desc_ua')}</label>
              <div class="col-md-9">
                <input id="name" name="desc_ua" type="text" placeholder="{Translation::getLabel('desc_ua')}" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">{Translation::getLabel('type')}</label>
              <div class="col-md-9">
                <input id="name" name="type" type="text" placeholder="{Translation::getLabel('type')}" class="form-control">
              </div>
            </div>
    
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">{Translation::getLabel('content_ru')}</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ru" placeholder="{Translation::getLabel('content_ru')}" rows="5"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message">{Translation::getLabel('content_ua')}</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ua" placeholder="{Translation::getLabel('content_ua')}" rows="5"></textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>