<hr/>
<form action="change_language.php" method="post">
  <select style="width: 100px" class="form-control" name = "language">
    <option {if $language eq 'ua'}selected{/if} value="ua">Ukrainian</option>
    <option {if $language eq 'ru'}selected{/if} value="ru">Russian</option>
  </select>
  <input style="margin-top: 10px"type="submit" class="btn btn-default" value="Change"/>
</form>
</body>
</html>