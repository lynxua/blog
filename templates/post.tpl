<div class="container">
	<div class="col-md-12">
	    <h1>{$title}</h1>
	    <p>{$content}</p>
    	<div>
			<div class="pull-right">
				<span class="label label-info">{$type}</span>
			</div>         
     	</div>   
	</div>
</div>