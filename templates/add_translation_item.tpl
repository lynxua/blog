<div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal">
          <fieldset>
            <legend class="text-center">{Translation::getLabel('add_post')}</legend>
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">{Translation::getLabel('content_ru')}</label>
              <div class="col-md-9">
                <textarea class="form-control" id="text_ru" name="text_ru" placeholder="{Translation::getLabel('content_ru')}" rows="5"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message">{Translation::getLabel('content_ua')}</label>
              <div class="col-md-9">
                <textarea class="form-control" id="text_ua" name="text_ua" placeholder="{Translation::getLabel('content_ua')}" rows="5"></textarea>
              </div>
            </div>
            
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                  <input type="button" id="clear" class="btn btn-primary btn-lg" value="{Translation::getLabel('stop_translation')}">
                  <input type="button" id="submit" class="btn btn-primary btn-lg" value="Submit">
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div>