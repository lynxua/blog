<h1>{$title}</h1>
<p>{$description}</p>
<div>
	<form action="post.php" method="GET">
		<button class="badge" type="submit">{Translation::getLabel('open_full')}</button>
		<input type="hidden" name="id" value="{$id}"/>
	</form>
	{if $as_admin eq TRUE}
		<form action="edit.php" method="GET">
			<button class="badge" type="submit">{Translation::getLabel('edit_post')}</button>
			<input type="hidden" name="id" value="{$id}"/>
		</form>
		<form action="delete.php" method="POST">
			<button class="badge" type="submit">{Translation::getLabel('delete_post')}</button>
			<input type="hidden" name="id" value="{$id}"/>
		</form>
	{/if}
	<div class="pull-right">
		<span class="label label-success">{$type}</span>
		<span class="badge">Posted {$date}</span>
	</div>         
</div>
<hr>