<?php
 require_once "PostItem.php";
 require_once "PostDao.php";
 require_once 'libs/Smarty.class.php';
 $title = 'Post';
 include 'header.php';
 $dao = new PostDAO();
 $post = $dao -> getPost($_GET['id']);
 $smarty = new Smarty();
 if(isset($_SESSION['language']) && $_SESSION['language']=='ru'){
 	$smarty->assign("title",$post->getTitleRu());
	$smarty->assign("content", $post ->getContent_ru());
 }else{
 	$smarty->assign("title",$post->getTitleUa());
	$smarty->assign("content", $post -> getContent_ua());
 }
 $smarty->assign("type", $post -> getType());
 $smarty->display('post.tpl');
 //echo $post -> __toString();
 include 'footer.php';
?>