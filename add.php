<?php
 require_once "PostItem.php";
 require_once "PostDao.php";
 require_once 'libs/Smarty.class.php';
 $title = 'Add post';
 include 'header.php';
 $dao = new PostDAO();
 if($_SERVER['REQUEST_METHOD']=='POST'){
 	$dao -> addPost($_POST['title_ru'], $_POST['title_ua'], $_POST['desc_ru'], $_POST['desc_ua'], $_POST['content_ru'], $_POST['content_ua'], $_POST['type']);
 	echo '<script type="text/javascript">'
   , 'window.location="../Blog/admin.php";'
   , '</script>';
}else{
	// $post = $dao -> getPost($_GET['id']);
	$smarty->assign("title_ru",'');
	$smarty->assign("content_ru", '');
	$smarty->assign("title_ua", '');
	$smarty->assign("content_ua", '');
	$smarty->assign("desc_ru", '');
	$smarty->assign("desc_ua", '');
	$smarty->assign("type", '');
	$smarty->display('add.tpl');
}
 include 'footer.php';
?>