<?php
require_once 'libs/Smarty.class.php';
require_once 'TranslationItem.php';
class PostDao
{
private $mysqli;
private static $connection;
	private function conOpen(){
        $this -> mysqli = new mysqli("localhost","root","", "blog");
    	$this -> mysqli -> query("SET NAMES 'utf8'");
    }
    private function query($sql){
        $result = $this -> mysqli -> query($sql);
            if(!$result){

            }else{      
                    return $result;
            }
    }

    private function conClose(){
         $this -> mysqli-> close();
    }

    private function getResult($query){
    	$this -> conOpen();
    	$result = $this -> mysqli -> query($query);
    	$this -> conClose();
    	return $result;
    }

    public function getAllPosts($as_admin){
		$rec_limit = 10;
		$this -> conOpen();
		$query = "SELECT count(id) FROM Post";
		$retval = $this -> mysqli -> query($query);
		if(! $retval )
		{
		  die('Could not get data: ' . mysql_error());
		}
		$row = mysqli_fetch_array($retval, MYSQL_NUM );
		$rec_count = $row[0];

		if( isset($_GET{'page'} ) )
		{
		   $page = $_GET{'page'} + 1;
		   $offset = $rec_limit * $page ;
		}
		else
		{
		   $page = 0;
		   $offset = 0;
		}
		$left_rec = $rec_count - ($page * $rec_limit);

		$query = "SELECT * FROM Post ORDER BY date DESC LIMIT $offset, $rec_limit";

		$retval = $this -> mysqli -> query($query);
		if(! $retval )
		{
		  die('Could not get data: ' . mysql_error());
		}
		//echo "<div class=\"container\">";
		//echo "<div class=\"col-md-12\">";
		
		while($row = mysqli_fetch_assoc($retval))
		{
			$post = new PostItem($row["id"],$row["date"], $row["title_ru"], $row["title_ua"], $row["desc_ru"],$row["desc_ua"],$row["content_ru"],$row["content_ua"], $row["type"]);
			$smarty = new Smarty();
			 if(isset($_SESSION['language']) && $_SESSION['language']=='ru'){
			 	$smarty->assign("title",$post->getTitleRu());
				$smarty->assign("description", $post ->getDesc_ru());
			 }else{
			 	$smarty->assign("title",$post->getTitleUa());
				$smarty->assign("description", $post -> getDesc_ua());
			 }
			 $smarty->assign("id", $post -> getId());
			 $smarty->assign("date", $post -> getDate());
			 $smarty->assign("type", $post -> getType());
			 $smarty->assign("as_admin", $as_admin);
			 $smarty->display('post_full.tpl');
		}
		if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1){
			echo "<a href=\"add.php\" class=\"badge\">".Translation::getLabel('add_post')."</a></br>";
			echo "<a href=\"upload.php\" class=\"badge\">".Translation::getLabel('upload_image')."</a></br>";
		}
		//echo "</div>";
		//echo "</div>";
		if($rec_count<=$rec_limit){

		}else if( $rec_count - $page*$rec_limit <= $rec_limit){
			$last = $page - 2;
		   echo "<a class=\"badge\" href=\"index.php?page=$last\">".Translation::getLabel('prev_10')."</a>";
		}else if( $page > 0 )
		{
		   $last = $page - 2;
		   echo "<a class=\"badge\" href=\"index.php?page=$last\">".Translation::getLabel('prev_10')."</a> |";
		   echo "<a  class=\"badge\" href=\"index.php?page=$page\">".Translation::getLabel('next_10')."</a>";
		}
		else if( $page == 0 )
		{
		   echo "<a class=\"badge\" href=\"index.php?page=$page\">".Translation::getLabel('next_10')."</a>";
		}
		// echo "</div>";
		// else if( $left_rec < $rec_limit )
		// {
		//    $last = $page - 2;
		//    echo "<a href=\"index.php?page=$last\">Last 10 Records</a>";
		// }
    }

    public function addPost($title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type){
		$this -> conOpen();
		$stmt = $this -> mysqli-> prepare("INSERT INTO Post (title_ru, title_ua, desc_ru, desc_ua, content_ru, content_ua, type) VALUES (?, ?, ?, ?, ?, ?, ?)") or trigger_error($this -> mysqli->error);
		if($stmt === FALSE){
			echo "pichalka";
		}
		else{
			$stmt -> bind_param("sssssss", $title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type);
			$stmt -> execute();
			$stmt -> close();
		}
		$this -> conClose();
    }

    public function getPost($id){
		$this -> conOpen();
		$post;
		$stmt = $this -> mysqli-> prepare("SELECT * FROM Post WHERE id = ?") or trigger_error($mysqli->error);
		if($stmt === FALSE){
			echo "pichalka";
		}
		else{
			$stmt -> bind_param("i", $id);
			$result = $stmt -> execute();
			$stmt -> bind_result($id, $date, $desc_ru, $desc_ua, $content_ru, $content_ua, $type, $title_ru, $title_ua);
			$stmt -> fetch();
      		$post = new PostItem($id, $date, $title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type);
			$stmt -> close();
		}
		$this -> conClose();
		return $post;
    }

    public function editPost($id, $title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type){
		$this -> conOpen();
		$stmt = $this -> mysqli-> prepare("UPDATE Post SET title_ru = ?, title_ua = ?, desc_ru = ?, desc_ua = ?, content_ru = ?, content_ua = ?, type = ? WHERE id = ?") or trigger_error($this ->mysqli->error);
		if($stmt === FALSE){
			echo "pichalka";
		}
		else{
			$stmt -> bind_param("sssssssi", $title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type, $id);
			$stmt -> execute();
			$stmt -> close();
		}
		$this -> conClose();
    }

    public function deletePost($id){
		$this -> conOpen();
		$stmt = $this -> mysqli-> prepare("DELETE FROM Post  WHERE id = ?") or trigger_error($this ->mysqli->error);
		if($stmt === FALSE){
			echo "pichalka";
		}
		else{
			$stmt -> bind_param("i", $id);
			$stmt -> execute();
			$stmt -> close();
		}
		$this -> conClose();
    }

    public function getTranslation(){
    	$this -> conOpen();
    	$query = "SELECT * FROM Translation ORDER BY date DESC";
		$retval = $this -> mysqli -> query($query);
		if(! $retval )
		{
		  die('Could not get data: ' . mysql_error());
		}
		if($retval->num_rows === 0)
	    {
	        echo '<h1 style="text-align: center">Translation is currently offline</h1>';
	    }
		while($row = mysqli_fetch_assoc($retval))
		{
			$post = new TranslationItem($row["id"],$row["date"], $row["text_ru"], $row["text_ua"]);
			$smarty = new Smarty();
			 if(isset($_SESSION['language']) && $_SESSION['language']=='ru'){
			 	$smarty->assign("text",$post->getTextRu());
			 }else{
			 	$smarty->assign("text",$post->getTextUa());
			 }
			 $smarty->assign("date", $post -> getDate());
			 //$smarty->assign("type", $post -> getType());
			 //$smarty->assign("as_admin", $as_admin);
			 $smarty->display('translation.tpl');
		}
		$this -> conClose();
    }

    public function addPostToTranslation($text_ru, $text_ua){
		$this -> conOpen();
		$stmt = $this -> mysqli-> prepare("INSERT INTO Translation (text_ru, text_ua) VALUES (?, ?)") or trigger_error($this -> mysqli->error);
		if($stmt === FALSE){
			echo "pichalka";
		}
		else{
			$stmt -> bind_param("ss", $text_ru, $text_ua);
			$stmt -> execute();
			$stmt -> close();
		}
		$this -> conClose();
    }

    public function clearTranslation(){
    	$this -> conOpen();
		$stmt = $this -> mysqli-> query("DELETE FROM Translation") or trigger_error($this ->mysqli->error);
		$stmt -> close();
		$this -> conClose();
    }
}
?>