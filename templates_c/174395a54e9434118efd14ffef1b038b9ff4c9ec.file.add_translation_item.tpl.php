<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-19 12:00:46
         compiled from "./templates/add_translation_item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:69497329155211fb8da8147-46629039%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '174395a54e9434118efd14ffef1b038b9ff4c9ec' => 
    array (
      0 => './templates/add_translation_item.tpl',
      1 => 1429437643,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '69497329155211fb8da8147-46629039',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55211fb8e3ad67_93274332',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55211fb8e3ad67_93274332')) {function content_55211fb8e3ad67_93274332($_smarty_tpl) {?><div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal">
          <fieldset>
            <legend class="text-center"><?php echo Translation::getLabel('add_post');?>
</legend>
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message"><?php echo Translation::getLabel('content_ru');?>
</label>
              <div class="col-md-9">
                <textarea class="form-control" id="text_ru" name="text_ru" placeholder="<?php echo Translation::getLabel('content_ru');?>
" rows="5"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message"><?php echo Translation::getLabel('content_ua');?>
</label>
              <div class="col-md-9">
                <textarea class="form-control" id="text_ua" name="text_ua" placeholder="<?php echo Translation::getLabel('content_ua');?>
" rows="5"></textarea>
              </div>
            </div>
            
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                  <input type="button" id="clear" class="btn btn-primary btn-lg" value="<?php echo Translation::getLabel('stop_translation');?>
">
                  <input type="button" id="submit" class="btn btn-primary btn-lg" value="Submit">
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div><?php }} ?>
