<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-19 11:57:03
         compiled from "./templates/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1382911085551fd59b9298f8-67270016%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62483df3f1c47f7c7647f9a678cac868fcdd5a48' => 
    array (
      0 => './templates/add.tpl',
      1 => 1429436690,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1382911085551fd59b9298f8-67270016',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_551fd59b9ac6e2_63027285',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_551fd59b9ac6e2_63027285')) {function content_551fd59b9ac6e2_63027285($_smarty_tpl) {?><div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="add.php" method="post">
          <fieldset>
            <legend class="text-center"><?php echo Translation::getLabel('edit_post');?>
</legend>
    
            <div class="form-group">
              <label class="col-md-3 control-label" for="name"><?php echo Translation::getLabel('title_ru');?>
</label>
              <div class="col-md-9">
                <input id="name" name="title_ru" type="text" placeholder="<?php echo Translation::getLabel('title_ru');?>
" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name"><?php echo Translation::getLabel('title_ua');?>
</label>
              <div class="col-md-9">
                <input id="name" name="title_ua" type="text" placeholder="<?php echo Translation::getLabel('title_ua');?>
" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name"><?php echo Translation::getLabel('desc_ru');?>
</label>
              <div class="col-md-9">
                <input id="name" name="desc_ru" type="text" placeholder="<?php echo Translation::getLabel('desc_ru');?>
" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name"><?php echo Translation::getLabel('desc_ua');?>
</label>
              <div class="col-md-9">
                <input id="name" name="desc_ua" type="text" placeholder="<?php echo Translation::getLabel('desc_ua');?>
" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name"><?php echo Translation::getLabel('type');?>
</label>
              <div class="col-md-9">
                <input id="name" name="type" type="text" placeholder="<?php echo Translation::getLabel('type');?>
" class="form-control">
              </div>
            </div>
    
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message"><?php echo Translation::getLabel('content_ru');?>
</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ru" placeholder="<?php echo Translation::getLabel('content_ru');?>
" rows="5"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message"><?php echo Translation::getLabel('content_ua');?>
</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ua" placeholder="<?php echo Translation::getLabel('content_ua');?>
" rows="5"></textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div><?php }} ?>
