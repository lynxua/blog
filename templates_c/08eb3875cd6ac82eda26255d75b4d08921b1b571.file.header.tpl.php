<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-19 12:53:30
         compiled from "./templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26902472955213b9c74fd04-90235479%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08eb3875cd6ac82eda26255d75b4d08921b1b571' => 
    array (
      0 => './templates/header.tpl',
      1 => 1429438948,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26902472955213b9c74fd04-90235479',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55213b9c80fa66_35276642',
  'variables' => 
  array (
    'title' => 0,
    'as_admin' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55213b9c80fa66_35276642')) {function content_55213b9c80fa66_35276642($_smarty_tpl) {?>	<!DOCTYPE html>
	<html>
	<head>
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 | Lynx's blog</title>
	<link rel="stylesheet" href="style/style.css"/>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
	</head>
	<body>
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="../Blog/index.php">Lynx's blog</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../Blog/admin.php"><?php echo Translation::getLabel('admin_panel');?>
<span class="sr-only">(current)</span></a></li>
        <li><a href="../Blog/translation.php"><?php echo Translation::getLabel('translation');?>
</a></li>
        <li><a href="../Blog/about.php"><?php echo Translation::getLabel('about');?>
</a></li>
      </ul>
      <?php if ($_smarty_tpl->tpl_vars['as_admin']->value==1) {?>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="exit.php"><?php echo Translation::getLabel('exit');?>
</a></li>
      </ul>
      <?php }?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav><?php }} ?>
