<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-04-04 14:07:01
         compiled from "./templates/edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1249255436551c61fab2b5d6-82352050%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a418b035c45c598d662c9526ca5d5514f7916ee4' => 
    array (
      0 => './templates/edit.tpl',
      1 => 1428149217,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1249255436551c61fab2b5d6-82352050',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_551c61fab95d03_37287976',
  'variables' => 
  array (
    'id' => 0,
    'title_ru' => 0,
    'title_ua' => 0,
    'desc_ru' => 0,
    'desc_ua' => 0,
    'type' => 0,
    'content_ru' => 0,
    'content_ua' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_551c61fab95d03_37287976')) {function content_551c61fab95d03_37287976($_smarty_tpl) {?><div class="container" style="margin-top: 20px">
	<div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well well-sm">
          <form class="form-horizontal" action="edit.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"/>
          <fieldset>
            <legend class="text-center">Edit post</legend>
            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Russian title</label>
              <div class="col-md-9">
                <input id="name" name="title_ru" type="text" placeholder="Russian title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['title_ru']->value;?>
">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Ukarainian title</label>
              <div class="col-md-9">
                <input id="name" name="title_ua" type="text" placeholder="Ukarainian title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['title_ua']->value;?>
">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Russian description</label>
              <div class="col-md-9">
                <input id="name" name="desc_ru" type="text" placeholder="Russian description" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['desc_ru']->value;?>
">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Ukrainian description</label>
              <div class="col-md-9">
                <input id="name" name="desc_ua" type="text" placeholder="Ukrainian description" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['desc_ua']->value;?>
">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="name">Type</label>
              <div class="col-md-9">
                <input id="name" name="type" type="text" placeholder="Type" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
              </div>
            </div>
    
    
            <!-- Message body -->
            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Russian content</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ru" placeholder="Content..." rows="5"><?php echo $_smarty_tpl->tpl_vars['content_ru']->value;?>
</textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="message">Ukrainian content</label>
              <div class="col-md-9">
                <textarea class="form-control" id="message" name="content_ua" placeholder="Content..." rows="5" ><?php echo $_smarty_tpl->tpl_vars['content_ua']->value;?>
</textarea>
              </div>
            </div>
    
            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
	</div>
</div><?php }} ?>
