<?php
class PostItem
{
private $id;
private $date;
private $title_ru;
private $title_ua;
private $desc_ru;
private $desc_ua;
private $type;
private $content_ru;
private $content_ua;

public function __construct($id, $date, $title_ru, $title_ua, $desc_ru, $desc_ua, $content_ru, $content_ua, $type)
{
	$this->id = $id;
	$this->date = $date;
	$this->title_ru = $title_ru;
	$this->title_ua = $title_ua;
	$this->desc_ru = $desc_ru;
	$this->desc_ua = $desc_ua;
	$this->content_ru = $content_ru;
	$this->content_ua = $content_ua;
	$this->type = $type;
}

public function getId()
{
	return $this->id;
}

public function getDate()
{
	return $this->date;
}

public function getTitleRu()
{
	return $this->title_ru;
}

public function getTitleUa()
{
	return $this->title_ua;
}

public function getDesc_ru()
{
	return $this->desc_ru;
}

public function getDesc_ua()
{
	return $this->desc_ua;
}

public function getContent_ru()
{
	return $this->content_ru;
}

public function getContent_ua()
{
	return $this->content_ua;
}

public function getType()
{
	return $this->type;
}

public function __toString()
{
	return "id = $this->id;<br /> 
	date = $this->date;<br /> 
	titleru = $this->title_ru;<br /> 
	titleua = $this->title_ua;<br /> 
	desc_ru = $this->desc_ru;<br /> 
	desc_ua = $this->desc_ua;<br /> 
	content_ru = $this->content_ru; <br />
	content_ua = $this->content_ua;<br /> 
	type = $this->type;<br /> <br /> ";
}
}
?>